﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MoneyTransfer.ViewModels
{
    public class TransferCreatedViewModel
    {
       
        public string Sender { get; set; }
        public string Receiver { get; set; }
        public DateTime DateCreated { get; set; }
        public double Count { get; set; }

    }
}
