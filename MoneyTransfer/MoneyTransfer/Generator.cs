﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using MoneyTransfer.Models;

namespace MoneyTransfer
{
    public class Generator
    {
      
            private readonly UserManager<User> _userManager;
            private ApplicationContext context;

            public Generator(ApplicationContext context)
            {
                this.context = context;
            }

            public Generator()
            {
            }

            public async Task<string> CodeGenerator()
            {

                Random rnd = new Random();

                string code = String.Empty;
                for (int i = 0; i < 6; i++)
                {

                    code = code + rnd.Next(0, 10);
                }

                bool result = await CheckingForUniqness(code);
                if (!result)
                {
                    await CodeGenerator();
                }

                return code;
            }


            public async Task<bool> CheckingForUniqness(string str)
            {

                if (await context.Users.FirstOrDefaultAsync(x => x.UniqCode == str) == null || context.Users == null)
                {
                    return true;
                }
                return false;
            }
        }
    }

