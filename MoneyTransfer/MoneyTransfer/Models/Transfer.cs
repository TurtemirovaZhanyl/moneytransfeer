﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace MoneyTransfer.Models
{
    public class Transfer
    {
        public int Id { get; set; }
        public string SenderId { get; set; }
        public User Sender { get; set; }
        public string ReceiverId { get; set; }
        public User Receiver { get; set; }
        [Required]
        [Display(Name = "ReceiverUniqCode")]
        [Remote(action: "CheckReceiverUniqCode", controller: "Transfer", ErrorMessage = "Неcуществующий код адресата ")]
        public string ReceiverUniqCode { get; set; }
        [Remote(action: "CheckCount", controller: "Transfer", ErrorMessage = "Недостаточно средств")]
        public double CountToSend { get; set; }
        public DateTime DateTransfer { get; set;}

    }
}
