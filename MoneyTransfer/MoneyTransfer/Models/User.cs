﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;

namespace MoneyTransfer.Models
{
    public class User : IdentityUser
    {
        public string UniqCode { get; set; }
        public double Count { get; set; }
    }
}
