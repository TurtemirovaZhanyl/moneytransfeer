﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Localization;
using MoneyTransfer.Controllers;
using MoneyTransfer.Models;
using MoneyTransfer.ViewModels;

namespace MoneyTransfer.Resources.Controllers
{
    public class UserController : Controller
    {
        private readonly IStringLocalizer<HomeController> _localizer;
        private readonly UserManager<User> _userManager;
        private ApplicationContext _context;
        public UserController(UserManager<User> userManager, IStringLocalizer<HomeController> localizer, ApplicationContext context)
        {
            _localizer = localizer;
            _userManager = userManager;
            _context = context;
        }

        public async Task<IActionResult> Index()
        {
            User user = await _userManager.Users.FirstOrDefaultAsync(x => x.UserName == User.Identity.Name);
            return View(user);
        }

       

        //public async Task<IActionResult> ViewUserCount()
        //{
        //    User user = await _userManager.Users.FirstOrDefaultAsync(x => x.UserName == User.Identity.Name);
        //    return Content(user.Count.ToString());
        //}
    }
}