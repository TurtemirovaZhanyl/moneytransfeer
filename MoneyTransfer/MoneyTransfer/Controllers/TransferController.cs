﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Localization;
using MoneyTransfer.Models;
using MoneyTransfer.ViewModels;

namespace MoneyTransfer.Controllers
{
    public class TransferController : Controller
    {
        private readonly IStringLocalizer<HomeController> _localizer;
        private readonly UserManager<User> _userManager;
        private ApplicationContext _context;
        public TransferController(UserManager<User> userManager, IStringLocalizer<HomeController> localizer, ApplicationContext context)
        {
            _localizer = localizer;
            _userManager = userManager;
            _context = context;
        }
        public IActionResult Index()
        {
            User user = _userManager.Users.FirstOrDefault(x => x.UserName == User.Identity.Name);
            List<Transfer> transferReceiver= _context.Transfers.Where(x=>x.ReceiverId==user.Id ).ToList();
            List<Transfer> transferSender= _context.Transfers.Where(x=>x.SenderId==user.Id ).ToList();
            List<Transfer> transferList = transferReceiver.Union(transferSender).ToList();
            return View(transferList);
        }
        public async Task<IActionResult> TransferMethod()
        {
            User user = await _userManager.Users.FirstOrDefaultAsync(x => x.UserName == User.Identity.Name);
            ViewBag.UserId = user.Id;
            return View();
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> TransferMethod(Transfer transfermodel)
        {
            User user = await _userManager.Users.FirstOrDefaultAsync(x => x.UserName == User.Identity.Name);
            User reciever = await _userManager.Users.FirstOrDefaultAsync(x => x.UniqCode == transfermodel.ReceiverUniqCode);
            if (ModelState.IsValid)

            {
                transfermodel.Sender = user;
                transfermodel.SenderId = user.Id;
                transfermodel.Receiver = reciever;
                transfermodel.ReceiverId = reciever.Id;
                transfermodel.DateTransfer = DateTime.Now;
            }
            user.Count = user.Count - transfermodel.CountToSend;
            reciever.Count = reciever.Count + transfermodel.CountToSend;
            await _context.Transfers.AddAsync(transfermodel);
            await _userManager.UpdateAsync(user);
            await _userManager.UpdateAsync(reciever);
            await _context.SaveChangesAsync();
            return View();
        }
        [AcceptVerbs("Get", "Post")]
        public IActionResult CheckCount(int CountToSend)
        {
            User user = _userManager.Users.FirstOrDefault(x => x.UserName == User.Identity.Name);
            return Json(CountToSend <= user.Count);
        }

        [AcceptVerbs("Get", "Post")]
        public IActionResult CheckReceiverUniqCode(string ReceiverUniqCode)
        {
            User user = _userManager.Users.FirstOrDefault(x => x.UniqCode == ReceiverUniqCode);
            return Json(user!=null);
        }
    }
}