﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using MoneyTransfer;
using MoneyTransfer.Controllers;
using Xunit;

namespace MoneyTransferTests
{
    public class GeneratorTests
    {
        [Fact]
        public async void CodeGeneratorTests()
        {
            Generator generator = new Generator();

            string a =  await generator.CodeGenerator();
            Assert.Equal(6, a?.Length);

        }


    }
}
